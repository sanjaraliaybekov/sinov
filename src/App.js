import React from 'react';
import Header from "./components/header/Header";
import MySection from "./components/section/mySection";
import MySection2 from "./components/section/MySection2";
import Footer from "./components/footer/Footer";
import BlueSection from "./components/section/BlueSection";
import Tesimonials from "./components/section/tesimonials";

function App(props) {

    return (
        <div>
            <Header/>
            <MySection/>
            <BlueSection/>
            <Tesimonials/>
            <MySection2/>
            <Footer/>
        </div>
    );
}


export default App;