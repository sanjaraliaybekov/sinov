import React, {useEffect, useState} from 'react';
import MyNavbar from "./MyNavbar";
import {Link} from "react-router-dom";
import AOS from 'aos';
import RubberBand from "react-reveal/RubberBand";
import {Tada} from 'react-reveal/Tada'

function Header(props) {
    useEffect(() => {
        AOS.init();
    });
    const [circlePosition, setCirclePosition] = useState(false);
    const [circlePosition2, setCirclePosition2] = useState(false);
    const [circlePosition3, setCirclePosition3] = useState(false);
    const [count, setCount] = useState(0);

    function removeCount() {
        if (count > 0) {
            setCount(count - 1);
        }
    }

    function addCount() {
        setCount(count + 1);
    }

    function isOnClick(a) {
        if (a === "1") {
            setCirclePosition(!circlePosition)
        } else if (a === "2") {
            setCirclePosition2(!circlePosition2)
        } else setCirclePosition3(!circlePosition3)
    }

    return (
        <div className="header position-relative">
            <MyNavbar/>
            <img src="/images/www.svg" className=" header-bg-img position-absolute" alt=""/>
            <div className="container header-container">
                <div className="row">
                    <div className="col-7">
                        <div className="header-text" data-aos="fade-down" data-aos-duration="1500">
                            <RubberBand delay={1500}><h1>Building exactly the eCommerce website you want.</h1>
                            </RubberBand>
                            <p>WooCommerce is a customizable, open-source eCommerce platform built on WordPress. <br/>
                                Get started quickly and make your way.</p>
                            <div className="d-flex align-items-center">
                                <button className="btn">Start New Store</button>
                                or <Link to="/">Customize & Extend ›</Link></div>
                        </div>
                    </div>
                    <div className="dots blue" style={{top: 330, zIndex: 5, right: 350, width: 500, height: 250}}/>
                    <div className="dots red-dot blue"
                         style={{top: 175, zIndex: 5, right: 0, width: 300, height: 250}}/>
                    <div className="dots red-dot blue purple"
                         style={{top: 805, zIndex: 5, right: 15, width: 100, height: 250}}/>
                    <div className="header-product bg-white p-0" data-aos="zoom-in" data-aos-duration="1500">
                        <div className="man-shoe" style={{zIndex: 15}}>
                            <button className="btn sale">Sale</button>
                            <img className="smiling-girls" data-aos="flip-left" data-aos-duration="1500"
                                 data-aos-delay="500" src="/images/smiling-girls.svg" alt=""/>
                            <button
                                className="btn backet position-absolute d-flex justify-content-center align-items-center">
                                <img src="/images/backet.svg" alt=""/>
                            </button>
                            <div className="stripe d-flex flex-column w-100" data-aos="flip-left"
                                 data-aos-duration="1500" data-aos-delay="500">
                                <div className="d-flex justify-content-between align-items-center">
                                    <img src="/images/Stripe-Logo 1.svg" alt=""/>
                                    <h6>Stripe gateway</h6>
                                    <button className="btn"
                                            style={circlePosition ? {backgroundColor: "#70c217"} : {backgroundColor: "#DAE5F7"}}
                                            onClick={() => isOnClick("1")}>
                                        <div style={circlePosition ? {right: "0", backgroundColor: "#ffffff"} : {
                                            left: "3",
                                            backgroundColor: "#8161F7"
                                        }}/>
                                    </button>
                                </div>
                                <div className="d-flex justify-content-between align-items-center">
                                    <img src="/images/PayPal-Logo 1.svg" alt=""/>
                                    <h6>Stripe gateway</h6>
                                    <button className="btn"
                                            style={circlePosition2 ? {backgroundColor: "#70c217"} : {backgroundColor: "#DAE5F7"}}
                                            onClick={() => isOnClick("2")}>
                                        <div style={circlePosition2 ? {right: "0", backgroundColor: "#ffffff"} : {
                                            left: "3",
                                            backgroundColor: "#8161F7"
                                        }}/>
                                    </button>
                                </div>
                                <div className="d-flex justify-content-between align-items-center">
                                    <img src="/images/home.svg" alt=""/>
                                    <h6>Stripe gateway</h6>
                                    <button className="btn"
                                            style={circlePosition3 ? {backgroundColor: "#70c217"} : {backgroundColor: "#DAE5F7"}}
                                            onClick={() => isOnClick("3")}>
                                        <div style={circlePosition3 ? {right: "0", backgroundColor: "#ffffff"} : {
                                            left: "3",
                                            backgroundColor: "#8161F7"
                                        }}/>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="order-product" data-aos="zoom-in" data-aos-duration="1500" data-aos-delay="500">
                            <div className="order">
                                <div className="text d-flex w-100"><p>Shoes</p><h6>Adidas Originals</h6></div>
                                <h2>Adidas Originals
                                    OG Sneakers
                                </h2>
                                <div className="order-footer">
                                    <div className="counters d-flex w-100">
                                        <button className="btn">7</button>
                                        <button className="btn">7.5</button>
                                        <button className="btn">8</button>
                                        <button className="btn">8.5</button>
                                        <button className="btn">9</button>
                                    </div>
                                    <div className="add-count d-flex">
                                        <div className="price">
                                            <h2>$344</h2>
                                            <del>$560</del>
                                        </div>
                                        <div className="counter">
                                            <div
                                                className="plus-minus d-flex justify-content-between align-items-center">
                                                <button className="btn" onClick={removeCount}><h6><b>-</b></h6></button>
                                                <h6>{count}</h6>
                                                <button className="btn" onClick={addCount}><h6><b>+</b></h6></button>
                                            </div>
                                        </div>
                                    </div>
                                    <button className="btn add-to-card w-100"><h6>Add to card</h6></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <h3 className="footer-title" data-aos="fade-up" data-aos-duration="1500">Your eCommerce <br/>
                made simple</h3>
        </div>
    );
}

export default Header;