import React, {useEffect, useState} from 'react';
import {Link} from "react-router-dom";
import {Collapse, Nav, Navbar, NavbarToggler, NavItem} from "reactstrap";


function MyNavbar(props) {
    const [isOpen, setIsOpen] = useState(false);
    const toggle = () => setIsOpen(!isOpen);
    return (
        <div className="header-nav">
            <Navbar light expand="md">
                <Link to="/" className="navbar-brand"><img src="/images/LOGO.svg" alt=""/></Link>
                <NavbarToggler onClick={toggle}/>
                <Collapse isOpen={isOpen} navbar>
                    <Nav className="mr-auto" navbar>
                        <NavItem>
                            <Link className="nav-link first" to="/sell">Sell</Link>
                        </NavItem>
                        <NavItem>
                            <Link className="nav-link first" to="/marketplace">Marketplace</Link>
                        </NavItem>
                        <NavItem>
                            <Link className="nav-link first" to="/community">Community</Link>
                        </NavItem>
                        <NavItem>
                            <Link className="nav-link first" to="/develop">Develop</Link>
                        </NavItem>
                        <NavItem>
                            <Link className="nav-link first" to="/resources">Resources</Link>
                        </NavItem>
                    </Nav>
                </Collapse>
                <Nav className="mr-auto" navbar>
                    <NavItem className="d-flex justify-content-center align-items-center">
                        <Link className="nav-link" to="/login">Log in</Link>
                    </NavItem>
                    <NavItem>
                        <Link className="nav-link" to="/get-started">
                            <button className="btn get-started">Get Started</button>
                        </Link>
                    </NavItem>
                    <NavItem className="d-flex justify-content-center align-items-center">
                        <Link className="nav-link search" to="/">
                            <img src="/images/icon-search.svg" alt=""/>
                        </Link>
                    </NavItem>
                </Nav>
            </Navbar>
        </div>
    );
}

export default MyNavbar;