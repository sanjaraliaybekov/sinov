import React from 'react';
import Typed from "react-typed";
import {Zoom} from "react-reveal";

function MySection2(props) {
    return (
        <div className="my-section-2">
            <div className="container position-relative">
                <div className="row">
                    <div className="col-12">
                        <Typed
                            strings={['']}
                            typeSpeed={70}
                            startDelay={500}/>
                        <div className="title text-center" data-aos="fade-up" data-aos-duration="1500">
                            <Zoom cascade><h3>Supported by real people</h3>
                                <p>Our team of Happiness Engineers works remotely from 58 countries
                                    providing <br/> customer
                                    support across multiple time zones.</p></Zoom>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12" style={{zIndex: 15}} data-aos="fade-up" data-aos-duration="1500">
                        <div className="persons">
                            <img src="/images/personse.svg" alt=""/>
                        </div>
                    </div>
                </div>
                <div className="circle w65" style={{top: 178, zIndex: 6, right: -50, width: 45, height: 45}}/>
                <div className="circle w65" style={{top: 186, zIndex: 6, left: -160}}/>
                <div className="circle w65" style={{top: 370, zIndex: 6, left: -90, width: 40, height: 40}}/>
                <div className="dots" style={{top: 410, zIndex: -11, left: -220, width: 500, height: 250}}/>
                <div className="dots red-dot"
                     style={{top: 310, zIndex: -11, right: -180, width: 250, height: 250}}/>
            </div>
            <div className="text-vs-button d-flex justify-content-center align-items-center" data-aos="fade-up"
                 data-aos-duration="1500">
                <h4>
                    WooCommerce - the most customizable eCommerce <br/> platform for building your online
                    business.
                </h4>
                <button className="btn get-started-btn">
                    <p>GET STARTED</p>
                </button>
            </div>
        </div>
    );
}

export default MySection2;