import React from 'react';
import {Link} from "react-router-dom";

function MySection(props) {
    return (
        <div className="my-section" data-aos="fade-up" data-aos-duration="1500">
            <div className="container">
                <div className="row">
                    <div className="info-box position-relative" data-aos="fade-down" data-aos-duration="1500">
                        <img className="pay position-absolute"
                             src="/images/kisspng-apple-pay-google-pay-apple-wallet-payment-5b074a74632137 1.svg"
                             alt=""/>
                        <img className="document position-absolute" src="/images/document.svg" alt=""/>
                        <div className="img">
                            <img src="/images/box-img1.svg" alt=""/>
                        </div>
                        <div className="title">
                            <h4>All You Need to Start</h4>
                        </div>
                        <div className="text">
                            <p>Add WooCommerce plugin to any WordPress site and set up a new store in minutes.</p>
                        </div>
                        <div className="link">
                            <p><b><Link to="/">
                                Ecommerce for Wordpress ›
                            </Link></b></p>
                        </div>
                    </div>
                    <div className="info-box position-relative">
                        <img className="square position-absolute" src="/images/square.svg" alt=""/>
                        <img className="facebook position-absolute" src="/images/facebook (2).svg" alt=""/>
                        <img className="google-adds position-absolute" src="/images/google-adds.svg" alt=""/>
                        <img className="jetbak position-absolute" src="/images/getpak.svg" alt=""/>
                        <img className="mankey position-absolute" src="/images/mankey.svg" alt=""/>
                        <div className="img">
                            <img src="/images/box-img2.svg" alt=""/>
                        </div>
                        <div className="title">
                            <h4>All You Need to Start</h4>
                        </div>
                        <div className="text">
                            <p>Add WooCommerce plugin to any WordPress site and set up a new store in minutes.</p>
                        </div>
                        <div className="link">
                            <p><b><Link to="/">
                                Browse Extensions ›
                            </Link></b></p>
                        </div>
                    </div>
                    <div className="info-box position-relative">
                        <img className="lastboximg1 position-absolute" src="/images/lastboximg1.svg" alt=""/>
                        <img className="lastboximg2 position-absolute" src="/images/lastboximg2.svg" alt=""/>
                        <img className="lastboximg3 position-absolute" src="/images/lastboximg3.svg" alt=""/>
                        <div className="img">
                            <img src="/images/boximg3.svg" alt=""/>
                        </div>
                        <div className="title">
                            <h4>All You Need to Start</h4>
                        </div>
                        <div className="text">
                            <p>Add WooCommerce plugin to any WordPress site and set up a new store in minutes.</p>
                        </div>
                        <div className="link">
                            <p><b><Link to="/">
                                Check our Forums ›
                            </Link></b></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    );
}

export default MySection;