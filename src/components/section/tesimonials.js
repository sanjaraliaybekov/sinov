import React from 'react';

function Tesimonials(props) {
    const [count, setCount] = React.useState(0)
    const [testimonials, setT] = React.useState([
        'You have not been paying attention Payin\' attention Payin\' attention Payin\' attention',
        'You have not been paying attention Payin\' attention Payin\' attention Payin\' attention',
        'Because you have not been Payin\' attention Payin\' attention Payin\' attention Payin\' attention',
        'It\'s the devil\'s way now There is no way out You can scream and you can shout It is too late now',
        'I\'ll lay down the tracks Sandbag and hide January has April\'s showers And two and two always makes up five',
        'Are you such a dreamer To put the world to rights? I\'ll stay home forever Where two and two always makes up five',
    ]);
    return (
        <div className="test">
            <div className='container' data-aos="fade-up" data-aos-duration="1500">
                <div className="d-flex text-center justify-content-center" data-aos="flip-right" data-aos-duration="1500">
                    <h3>Trusted by Agencies <br/> & Store Owners</h3>
                </div>
                <div className="row position-relative">
                    <div className="position-absolute">
                        <img className='position-absolute t1' src="/images/t1.png" alt=""/>
                        <img className='position-absolute t2' src="/images/t2.png" alt=""/>
                        <img className='position-absolute t3' src="/images/t3.png" alt=""/>
                        <img className='position-absolute t4' src="/images/t4.png" alt=""/>
                        <img className='position-absolute t5' src="/images/t5.png" alt=""/>
                        <img className='position-absolute t6' src="/images/t6.png" alt=""/>
                    </div>
                    <div className="dots" style={{top: 470, left: 200, width: 500, height: 250}}/>
                    <div className="dots red-dot" style={{top: 100, right: 200, width: 250, height: 250}}/>
                    <div className="to-left position-absolute" onClick={() => setCount(count + 1)}
                         style={{top: 660, left: 970}}>
                        <img src="/images/rl.svg" alt=""/>
                    </div>
                    <div className="to-left position-absolute" onClick={() => setCount(count + 1)}
                         style={{top: 660, left: 910}}>
                        <img style={{transform: 'rotate(180deg)'}} src="/images/rl.svg" alt=""/>
                    </div>
                    <div className="offset-3 col-6">
                        <div className="carusel" data-aos="zoom-in" data-aos-duration="1500">
                            {testimonials.map((item, index) => (
                                <div style={{
                                    transform: ((index + count) % 6) !== 0 ? 'translateZ(' + (((index + count) % 6) * 10) + 'px) translateY(' + (150 + ((index + count) % 6) * -30) + 'px)' : '',
                                    zIndex: ((index + count) % 6) !== 0 ? ((index + count) % 6) : ''
                                }} className={'cards card' + ((index + count) % 6)}>
                                    {item}
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Tesimonials;