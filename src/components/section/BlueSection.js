import React from 'react';
import {Zoom} from "react-reveal";

function BlueSection(props) {
    return (
        <div className="blue-section">
            <img src="/images/woo.svg" style={{position: 'absolute', zIndex: 15, right: 0}} alt=""/>
            <img style={{zIndex: 2}} className="pillow-img" src="/images/pillow.svg" alt=""/>
            <div className="container h100vh d-flex flex-column justify-content-between">
                <div style={{zIndex: 3}} className="row position-relative">
                    <div className="circle" style={{top: 191, left: 10}}/>
                    <div className="circle w65" style={{top: 146}}/>
                    <div className="dots" style={{top: 395}}/>
                    <div className="offset-1 col-4" data-aos="flip-left" data-aos-duration="1500">
                        <img className='img-gb' style={{transform: 'translateY(-19px)'}}
                             src="/images/girls.png"
                             alt=""/>
                    </div>
                    <div className="col-4" style={{marginTop: 97}}>
                        <h4 className='text-white'>Develop <br/> Without Limits</h4>
                        <p className='text-white'><Zoom right opposite cascade>WooCommerce is developer friendly, too.
                            Built with a REST API,
                            WooCommerce is scalable and can integrate with virtually any
                            service. Design a complex store from scratch, extend a store for a
                            client, or simply add a single product to a WordPress site—your
                            store, your way.</Zoom></p>
                        <button className='btn btn-green rounded-pill'>Read the Documentation</button>
                    </div>
                </div>
                <div style={{zIndex: 3}} className="row position-relative">
                    <div className="circle" style={{top: -26, right: 0}}/>
                    <div className="circle w65" style={{top: -48, zIndex: 6, right: 400}}/>
                    <div className="dots" style={{right: -54, top: 190, height: 250}}/>
                    <div className="offset-3 col-4" style={{marginTop: 97}}>
                        <h4 className='text-white'>Know our <br/> Global Community</h4>
                        <p className='text-white'><Zoom right opposite cascade>WooCommerce is one of the fastest-growing
                            eCommerce communities.
                            We’re proud that the helpfulness of the community and a wealth of
                            online resources are frequently cited as reasons our users love
                            it. There are 80+ meetups worldwide!</Zoom></p>
                        <button className='btn btn-green rounded-pill'>Read the Documentation</button>
                    </div>
                    <div className="col-4" style={{zIndex: 5}}>
                        <img className='float-end img-gb' data-aos="flip-left" data-aos-duration="1500"
                             src="/images/boys.png" alt=""/>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default BlueSection;