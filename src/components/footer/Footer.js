import React from 'react';
import {Link} from "react-router-dom";
import {Zoom} from "react-reveal";

function Footer(props) {
    return (
        <div className="footer">
            <div className="container position-relative">
                <div className="row">
                    <div className="col-12">
                        <div className="support d-flex justify-content-center">
                            <div className="img-text d-flex align-items-center" data-aos="zoom-in" data-aos-delay="0"
                                 data-aos-duration="1500"><img src="/images/ptichka.svg" alt=""/>
                                <h5>30 day <b>money back guarantee</b></h5></div>
                            <div className="img-text d-flex align-items-center" data-aos="zoom-in" data-aos-delay="1000"
                                 data-aos-duration="1500"><img src="/images/ball (2).svg" alt=""/>
                                <h5><b>Support</b> teams across the world</h5></div>
                            <div className="img-text d-flex align-items-center" data-aos="zoom-in" data-aos-delay="500"
                                 data-aos-duration="1500"><img src="/images/some.svg" alt=""/>
                                <h5><b>Safe & Secure</b> online payment</h5></div>
                        </div>
                        <div className="circle w65" style={{top: -90, zIndex: 6, right: 0}}/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12">
                        <div className="logo text-center" data-aos="flip-left" data-aos-duration="1500">
                            <img src="/images/LOGO2.svg" alt=""/>
                            <hr/>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12 pb-4">
                        <div className="footer-links d-flex justify-content-center">
                            <Zoom cascade>
                                <div>
                                    <p>Who we Are</p>
                                    <ul>
                                        <li><Link to="/">About</Link></li>
                                        <li><Link to="/">Team</Link></li>
                                        <li><Link to="/">Work With Us</Link></li>
                                    </ul>
                                </div>
                                <div>
                                    <p>Woocommerce</p>
                                    <ul>
                                        <li><Link to="">Features</Link></li>
                                        <li><Link to="">Payments
                                        </Link></li>
                                        <li><Link to=""> Marketing
                                        </Link></li>
                                        <li><Link to="">Shipping
                                        </Link></li>
                                        <li><Link to=""> Extension Store
                                        </Link></li>
                                        <li><Link to="">eCommerce blog
                                        </Link></li>
                                        <li><Link to="">Development blog
                                        </Link></li>
                                        <li><Link to=""> Ideas board
                                        </Link></li>
                                        <li><Link to="">Mobile App
                                        </Link></li>
                                        <li><Link to=""> Community
                                        </Link></li>
                                        <li><Link to="">
                                            Style Guide
                                        </Link></li>
                                        <li><Link to="">
                                            Email Newsletter</Link></li>
                                    </ul>
                                </div>
                                <div>
                                    <p>Other products</p>
                                    <ul>
                                        <li><Link to="/">Storefront
                                        </Link></li>
                                        <li><Link to="/"> WooSlider
                                        </Link></li>
                                        <li><Link to="/"> Sensei
                                        </Link></li>
                                        <li><Link to="/">Sensei Extensions</Link></li>
                                    </ul>
                                </div>
                                <div>
                                    <p>Support</p>
                                    <ul>
                                        <li><Link to="">Documentation
                                        </Link></li>
                                        <li><Link to="">Customizations
                                        </Link></li>
                                        <li><Link to="">Support Policy
                                        </Link></li>
                                        <li><Link to=""> Contact
                                        </Link></li>
                                        <li><Link to="">COVID-19 Resources
                                        </Link></li>
                                        <li><Link to=""> Privacy Notice for
                                        </Link></li>
                                        <li><Link to="">California Users</Link></li>
                                    </ul>
                                </div>
                                <div>
                                    <p>We recommend</p>
                                    <ul>
                                        <li><Link to="">WooExperts
                                        </Link></li>
                                        <li><Link to=""> Hosting Solutions
                                        </Link></li>
                                        <li><Link to="">Pre-sales FAQ
                                        </Link></li>
                                        <li><Link to="">Success Stories
                                        </Link></li>
                                        <li><Link to="">Design Feedback Group</Link></li>
                                    </ul>
                                </div>
                            </Zoom>
                        </div>
                    </div>
                </div>
            </div>
            <div className="container-fluid laster-bg bg-light py-4">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <div className="laster d-flex justify-content-between">
                                <div className="d-flex">
                                    <a href="/"><img src="/images/twitter (2).svg" alt=""/></a>
                                    <a href="/"><img src="/images/facebook (3).svg" alt=""/></a>
                                    <a href="/"><img src="/images/wifi.svg" alt=""/></a>
                                    <a href="/"><img src="/images/instagram (2).svg" alt=""/></a>
                                </div>
                                <h6>COPYRIGHT WOOCOMMERCE 2020 - TERMS & CONDITIONS PRIVACY POLICY</h6>
                                <img src="/images/automatic.svg" alt=""/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Footer;